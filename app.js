const express = require('express');
// const config = require('./config').get('bot');
// const routes = require('./routes');
// const middlewares = require('./middlewares');

const bot = require('./bot');

const app = express();

// app.use(middlewares);

// app.use(routes);


app.set('port', process.env.PORT || 5555);
const port = app.get('port');

app.listen(port, () => {
  console.log(`Bot server ${port}`);
});

