const controller = require('../controllers/commandController');
const logger = require('../services/logger');

module.exports = (bot) => {
  bot.onText(/^\/start/, (msg) => {
    logger(msg.from.id, msg.text);
    controller.onStart(bot, msg);
  });
  bot.onText(/^\/help/, (msg) => {
    logger(msg.from.id, msg.text);
    controller.onHelp(bot, msg);
  });
  bot.onText(/^\/stop/, (msg) => {
    logger(msg.from.id, msg.text);
    controller.onStop(bot, msg);
  });
};
