const controllers = require('../controllers/bottomButtons');
const logger = require('../services/logger');


const onMessageHandlers = {
  'Мои вагоны': controllers.onList,
};

module.exports = (bot) => {
  bot.on('message', (msg) => {
    logger(msg.from.id, msg.text);
    if (msg.text.startsWith('/')) {
      return;
    }

    const handler = onMessageHandlers[msg.text];
    if (!handler) {
      controllers.onAdd(bot, msg);
      return;
    }
    handler(bot, msg);
  });
};
