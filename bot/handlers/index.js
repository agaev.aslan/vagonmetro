const messageHandlers = require('./messageHandlers');
const commandHandler = require('./commandHandlers');

module.exports = (bot) => {
  messageHandlers(bot);
  commandHandler(bot);
};
