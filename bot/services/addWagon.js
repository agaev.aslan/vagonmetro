const UserWagons = require('../../models').UserWagons;
const UserStory = require('../../models').UserStory;
const Wagon = require('../../models').Wagon;

async function addWagon(userId, msgText) {
    const wagonNum = msgText.split(' ')[0];
    const storyText = msgText.split(' ').slice(1).join(' ');
    try {
        const wagon = { id: wagonNum }
        Wagon.findOne({ where: { id: wagonNum } })
        .then((result) => {
           if (result) return;
           Wagon.create(wagon).catch(e => console.log(e));
       })
       .catch(e => console.log(e));
    } catch (error) {
        console.log('Wagon was not created', error)
    }

    try {
        userWagon = {
            userId: userId,
            wagonId: wagonNum,
         }
         UserWagons.findOne({ where: { id: userId } })
         .then((result) => {
            if (result) return;
            UserWagons.create(userWagon)
            .then(() => {
                if (typeof storyText !== 'undefined') {
                    try {
                        story = {
                            userId: userId,
                            wagonId: wagonNum,
                            storyText: storyText,
                         }
                        UserStory.create(story).catch(e => console.log(e));
                    } catch (error) {
                        console.log('error', error)
                    }
                }
            })
            .catch(e => console.log(e));
        })
        .catch(e => console.log(e));
    } catch (error) {
        console.log('error', error)
    }
}

module.exports = {
    addWagon,
};