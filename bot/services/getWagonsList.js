const UserWagons = require('../../models').UserWagons;

async function getWagonsList(userId) {
    const list = await UserWagons.findAll({ where: { userId: userId } });
    // console.log('service wagons', list);
    return list;
}

module.exports = {
    getWagonsList,
  };