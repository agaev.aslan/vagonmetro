const UserStory = require('../../models').UserStory;

async function getStoriesList(wagonId) {
    const list = await UserStory.findAll({ where: { wagonId: wagonId } });
    return list;
}

module.exports = {
    getStoriesList,
  };