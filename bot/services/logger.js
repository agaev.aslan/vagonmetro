const UserAction = require('../../models').UserActions;
const LastUserAction = require('../../models').UserLastActions;

function updateLastUserAction(userId, action) {
  LastUserAction.findOne({ where: { userId } })
    .then((result) => {
      if (result) {
        LastUserAction.update(
          { action },
          { where: { userId } },
        );
        return;
      }
      LastUserAction.create({ userId, action }).catch(e => console.log(e));
    })
    .catch(e => console.log(e));
}

function logUserAction(userId, action) {
  updateLastUserAction(userId, action);
  UserAction.create({
    userId,
    action,
  }).catch(e => console.log(e));
}

module.exports = logUserAction;
