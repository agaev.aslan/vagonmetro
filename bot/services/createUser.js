const User = require('../../models').User;

module.exports = (user) => {
  User.findOne({ where: { id: user.id } })
    .then((result) => {
      if (result) return;
      User.create(user).catch(e => console.log(e));
    })
    .catch(e => console.log(e));
};
