const getWagonsList = require('../../services/getWagonsList');
const addNewWagon = require('../../services/addWagon');
const getStories = require('../../services/getStories');

const keyboards = require('../../keyboards');

const returnPhrase = 'Приходите немного позже!';

async function onList(bot, msg) {
  let wagonsList = await getWagonsList.getWagonsList(msg.from.id);
  const options = {
    parse_mode: 'html',
    disable_web_page_preview: true,
    reply_markup: keyboards.default_reply_keyboard_markup,
  };
  let outputs = '';
  wagonsList.forEach(element => {
    outputs += `${element.dataValues.wagonId} -- ${new Date(element.dataValues.createdAt).getDate()}.${new Date(element.dataValues.createdAt).getMonth()+1}.${new Date(element.dataValues.createdAt).getFullYear()} \n`
  });
  bot.sendMessage(msg.chat.id, `Вагоны, в которых Вы были:\n${outputs}`, options);
}

async function onAdd(bot, msg) {
    const options = {
        parse_mode: 'html',
        disable_web_page_preview: true,
        reply_markup: keyboards.default_reply_keyboard_markup,
      };
    let storiesOuput = `Истории пользователей про вагон ${msg.text.split(' ')[0]}:\n`
    addNewWagon.addWagon(msg.from.id, msg.text).then(() => {
        getStories.getStoriesList(msg.text.split(' ')[0]).then((res) => {
          res.forEach(element => {
            storiesOuput += `${element.dataValues.storyText}\n`;
          })
        })
        const text = `Вагон ${msg.text.split(' ')[0]} успешно добавлен!`
        bot.sendMessage(msg.chat.id, text, options).then(() => {
          bot.sendMessage(msg.chat.id, storiesOuput, options);
        })
    });
}


module.exports = {
  onList,
  onAdd,
};

