const path = require('path');
const keyboards = require('../keyboards');
const writeUserToDB = require('../services/createUser');

function onStart(bot, msg) {
  writeUserToDB(msg.from, 3);
  const options = {
    reply_markup: keyboards.default_reply_keyboard_markup,
  };
  const startMessage = `Этот бот нужен для того, чтобы вести учёт вагонов метро, в которых вы бывали. 
  Чтобы добавить вагон просто отправьте боту номер вагона. Если вы хотите добавить историю, связанную с этим вагоном, напечатайте текст истории после номера вагона через проблел. Также, вы можете просмотреть истории других пользователей, произошедшие в этом вагоне.
  Чтобы посмотреть список вагонов, нажмите "Мои вагоны".`;
  bot.sendMessage(msg.chat.id, startMessage, options);
}

function onHelp(bot, msg) {
  const helpMessage = `Этот бот нужен для того, чтобы вести учёт вагонов метро, в которых вы бывали. 
Чтобы добавить вагон просто отправьте боту номер вагона. Также, вы можете просмотреть истории других пользователей, произошедшие в этом вагоне.
Чтобы посмотреть список вагонов, нажмите "Мои вагоны".`;
  const options = {
    reply_markup: keyboards.default_reply_keyboard_markup,
  };
  bot.sendMessage(msg.chat.id, helpMessage, options);
}


module.exports = {
  onStart,
  onHelp,
  // onStop,
};

