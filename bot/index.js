const TelegramBot = require('node-telegram-bot-api');
const handlers = require('./handlers');
const Agent = require('socks5-https-client/lib/Agent');

var bot; 

if (process.env.M === 'dev') {
    const token = require('../config').get('bot:token');
    bot = new TelegramBot(token, {
        polling: true,
        request: {
            agentClass: Agent,
            agentOptions: {
                socksHost: "127.0.0.1",
                socksPort: 9050,
                // If authorization is needed:
                // socksUsername: process.env.PROXY_SOCKS5_USERNAME,
                // socksPassword: process.env.PROXY_SOCKS5_PASSWORD
            }
        }
    });
    handlers(bot);

} else {
    const token = process.env.TOKEN;

    bot = new TelegramBot(token, {
        polling: true,
    });
    handlers(bot);
}


module.exports = bot;
