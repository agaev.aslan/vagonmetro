module.exports = (sequelize, DataTypes) => {
    const Wagon = sequelize.define('Wagon', {
      id: {
          primaryKey: true,
          type: DataTypes.STRING,
          required: true,
          unique: true,
      },
    });
    return Wagon;
  };
