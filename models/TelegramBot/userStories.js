module.exports = (sequelize, DataTypes) => {
    const UserStories = sequelize.define('UserStories', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      userId: DataTypes.INTEGER,
      wagonId: DataTypes.STRING,
      storyText: DataTypes.TEXT,
    });
    return UserStories;
};
