module.exports = (sequelize, DataTypes) => {
    const UserWagons = sequelize.define('UserWagons', {
      id: {
          primaryKey: true,
          type: DataTypes.INTEGER,
          autoIncrement: true
      },
      userId: DataTypes.INTEGER,
      wagonId: DataTypes.STRING,
    });
    return UserWagons;
  };
