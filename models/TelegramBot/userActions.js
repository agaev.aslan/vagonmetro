module.exports = (sequelize, DataTypes) => {
    const UserActions = sequelize.define('UserActions', {
      userId: DataTypes.INTEGER,
      action: DataTypes.STRING,
    });
    return UserActions;
  };
