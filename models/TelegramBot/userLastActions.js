module.exports = (sequelize, DataTypes) => {
    const UserLastActions = sequelize.define('UserLastActions', {
      userId: {
        primaryKey: true,
        type: DataTypes.INTEGER,
      },
      action: DataTypes.STRING,
    });
    return UserLastActions;
};