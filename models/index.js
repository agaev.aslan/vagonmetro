const Sequelize = require('sequelize');
const config = require('../config');

var connectionStringBot;

if (process.env.M === 'dev') {
  connectionStringBot = config.get('database:connectionStringBot');
} else {
  connectionStringBot =  process.env.DATABASE_URL;
}

const sequelize = new Sequelize(connectionStringBot, {
  logging: false,
  dialect: 'postgres',
  protocol: 'postgres',
  dialectOptions: {
    ssl: true
  },
  pool: {
    max: 5,
    min: 0,
    idle: 20000,
    acquire: 200000,
  },
});


const models = {
  User: require('./TelegramBot/user')(sequelize, Sequelize),
  Wagon: require('./TelegramBot/wagon')(sequelize, Sequelize),
  UserWagons: require('./TelegramBot/userWagons')(sequelize, Sequelize),
  UserActions: require('./TelegramBot/userActions')(sequelize, Sequelize),
  UserLastActions: require('./TelegramBot/userLastActions')(sequelize, Sequelize),
  UserStory: require('./TelegramBot/userStories')(sequelize, Sequelize),
};


function syncModels(models) {
  return new Promise((res, rej) => {
    let count = 0;
    Object.keys(models).forEach((modelName) => {
      models[modelName].sync();
      count++;
      if (count === Object.keys(models).length){
        res(count);
      }
    });
  });
}

syncModels(models).then(() => {
  console.log('synced');
});

models.User.hasMany(models.UserWagons, {
    foreignKey: 'userId',
    sourceKey: 'id',
    onDelete: 'cascade',
    hooks: true
});

models.Wagon.hasMany(models.UserStory, {
  foreignKey: 'wagonId',
  sourceKey: 'id',
  onDelete: 'cascade',
  hooks: true
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = models;
